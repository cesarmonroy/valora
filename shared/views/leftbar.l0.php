<aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo "../userspace/$profile_image"; ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $usuario;?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- search form -->

          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">Menú</li>
            <li class="treeview">
              <a href="main.php">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span></i>
              </a>
              
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-th"></i>
                <span>Inventario</span>
                <span class="label label-primary pull-right">4</span>
              </a>
              <ul class="treeview-menu">
                
                  <li><a href="./autores.list.php" target="target"><i class="fa fa-circle-o"></i> Autores</a></li>
                  <li><a href="./productos.list.php" target="target"><i class="fa fa-circle-o"></i> Productos</a></li>
                
              </ul>
            </li>
            
            <li class="treeview">
              <a href="#">
                <i class="fa fa-th"></i>
                <span>Compras</span>
                <span class="label label-primary pull-right">4</span>
              </a>
              <ul class="treeview-menu">
                
                  <li><a href="#" target="target"><i class="fa fa-circle-o"></i> Ordenes de Compra</a></li>
                  <li><a href="#" target="target"><i class="fa fa-circle-o"></i> Proveedores</a></li>
                
              </ul>
            </li>
            
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>