<?PHP

/*
	******************************************************************************
        Framework D-Parts 1.0
	J. César Monroy Fernández. México 2007-2010
        devahil@gmail.com
	******************************************************************************


*/

// Definición de Host y Base de Datos de Aplicación
/*
	Estos elementos pueden ser modificados a su gusto dependiendo de los requerimientos de su Aplicación.
*/
$servidor = "127.0.0.1"; // Host donde se encuentra el Servidor de Base de Datos
$usuario = "valora_user"; // Nombre de Usuario privilegiado para la base de datos
$password = "valora_password"; // Contraseña del Usuario
$base = "valora_main"; // Nombre de la base de datos que contendrá la información básica de la aplicación.

$table_users = "usuarios";
$usuarios_sesion="autentificator";

?>
