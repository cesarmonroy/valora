<?php

require_once '../../core/dpcore.config.php';
require_once '../../core/dpcore.crypt.php';
require_once '../../core/dpcore.db.php';
require_once '../../applibs/coreAPI.php';

$conf = new coreconfig();
$data = new coredb();
$cry = new corecrypt();
$core = new corelogic();

$string = "Test Message";
echo $string."<BR>";
$enc = $cry->encrypt($string);
echo "Cifrado: ".$enc."<BR>";
$dec = $cry->decrypt($enc);
echo "Natural: ".$dec."<BR>";

$token = $core->create_token();
echo $token;
?>

<script>
    window.location.hash = "qwertyuisdfghj";
</script>