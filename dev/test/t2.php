<!DOCTYPE html>
<html lang="es">
 <head>
     <meta charset="utf-8"/>
 <title>Creamos y mostramos la sesión</title>
 </head>
 <body>
 <div class="c1">
 <h2>Mostramos los datos guardados</h2>
 <section>
 <p>
     Tu nombre de usuario es: <?php echo $_SESSION['usuario_login'];?><br>
     Tu nivel de usuario es: <?php echo $_SESSION['usuario_nivel'];?><br>
     Tu ID de usuario es: <?php echo $_SESSION['usuario_id'];?><br>
 </p>
 </section>
 </div>
 
 <div class="c2">
 <section>
 <p>
     <a href="../logout.php">Eliminar sesión</a> <!-- de esta forma se crea la nueva session, sin necesidad de crear otro script en php. -->
 </p>
 </section>
 </div>
 </body>
</html>