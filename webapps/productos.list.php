<?php
    
    require_once '../applibs/dpAPI.php';
    require_once '../applibs/coreAPI.php';

    $conf = new coreconfig();
    $cry = new corecrypt();
    $data = new coredb();
    $log = new corelog();
    $mview = new setviews();
    $usuario = $_SESSION['usuario_login'];
    
        $nivel_acceso=1;
	if ($nivel_acceso <= $_SESSION['usuario_nivel']){
		header ("Location: $redir?error_login=5");
		exit;
	}
        
?>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../shared/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="../shared/plugins/datatables/dataTables.bootstrap.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../shared/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../shared/dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  
<div class="box">
                <div class="box-header">
                    <h3 class="box-title">Productos</h3>
                  <ul class="nav"><li class="dropdown">
                    <a aria-expanded="true" class="dropdown-toggle" data-toggle="dropdown" href="#">
                      Acciones especiales <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#" target="target">Inventario global</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="tipo_producto.php" target="target">Revisar tipos de producto</a></li> 
                    </ul>
                  </li>
                    </ul>
                </div><!-- /.box-header -->
                
                <div align="right">
                    
                    
                    
                    <h4><a href="crear.producto.php" target="target" border="0"><span class="label label-primary"> <span class="fa fa-fw fa-plus"></span> Agregar</span></a> &nbsp;
                        <a href="javascript:window.print()" border="0"><span class="label label-primary"> <span class="glyphicon glyphicon-print"></span> Imprimir</span></a> &nbsp;
                        <a href="export.productos.php"><span class="label label-success"> <span class="glyphicon glyphicon-download"></span> Descargar</span></a></h4>
                </div> 
                <div class="box-body">
                  <div class="dataTables_wrapper form-inline dt-bootstrap" id="example2_wrapper"><div class="row"><div class="col-sm-6"></div><div class="col-sm-6"></div></div><div class="row"><div class="col-sm-12"><table aria-describedby="example2_info" role="grid" id="example2" class="table table-bordered table-hover dataTable">
                    <thead>
                      <tr role="row"><th aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending" colspan="1" rowspan="1" aria-controls="example2" tabindex="0" class="sorting_asc">ID</th>
                          <th  colspan="1" rowspan="1" aria-controls="example2" tabindex="0" class="sorting">Autor</th>
                          <th  colspan="1" rowspan="1" aria-controls="example2" tabindex="0" class="sorting">Tipo</th>
                          <th  colspan="1" rowspan="1" aria-controls="example2" tabindex="0" class="sorting">Descripción</th>
                          <th  colspan="1" rowspan="1" aria-controls="example2" tabindex="0" class="sorting">Status</th>
                          <th  colspan="1" rowspan="1" aria-controls="example2" tabindex="0" class="sorting">TOKEN</th>
                          <th  colspan="1" rowspan="1" aria-controls="example2" tabindex="0" class="sorting">Costo</th>
                          <th  colspan="1" rowspan="1" aria-controls="example2" tabindex="0" class="sorting">P. mayoreo</th>
                          <th  colspan="1" rowspan="1" aria-controls="example2" tabindex="0" class="sorting">P. menudeo</th>
                          <th  colspan="1" rowspan="1" aria-controls="example2" tabindex="0" class="sorting">F. alta</th>
                          <th  colspan="1" rowspan="1" aria-controls="example2" tabindex="0" class="sorting">F. baja</th>
                      </tr>
                    </thead>
                    <tbody>
                                                

                      
                     <?php
              
                        $result = $data->query("SELECT id,id_autor,tipo_producto,descripcion,status,token,costo,precio_mayoreo,precio_menudeo,f_alta,f_baja FROM productos ORDER BY ID ASC");
                        while( $array_datos = mysql_fetch_array($result)){
                            $autor=$data->extract_data("SELECT nombre FROM autores WHERE id LIKE '$array_datos[1]'")." ".$data->extract_data("SELECT apellidos FROM autores WHERE id LIKE '$array_datos[1]'");
                             echo "<tr class='odd' role='row'><td class='sorting_1'><a href=producto.modificar.php?id=$array_datos[0]><span class='glyphicon glyphicon-edit'></span> $array_datos[0]</td><td>$array_datos[1]</td><td>$array_datos[2]</td><td>$array_datos[3]</td><td>$array_datos[4]</td><td>$array_datos[5]</td><td>$array_datos[6]</td><td>$array_datos[7]</td><td>$array_datos[8]</td><td>$array_datos[9]</td><td>$array_datos[10]</td></tr>";
                        }

                      ?>
                    
                    
                    </tbody>
                    <tfoot>
                        <tr role="row"><th aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending" colspan="1" rowspan="1" aria-controls="example2" tabindex="0" class="sorting_asc">ID</th>
                          <th  colspan="1" rowspan="1" aria-controls="example2" tabindex="0" class="sorting">Autor</th>
                          <th  colspan="1" rowspan="1" aria-controls="example2" tabindex="0" class="sorting">Tipo</th>
                          <th  colspan="1" rowspan="1" aria-controls="example2" tabindex="0" class="sorting">Descripción</th>
                          <th  colspan="1" rowspan="1" aria-controls="example2" tabindex="0" class="sorting">Status</th>
                          <th  colspan="1" rowspan="1" aria-controls="example2" tabindex="0" class="sorting">TOKEN</th>
                          <th  colspan="1" rowspan="1" aria-controls="example2" tabindex="0" class="sorting">Costo</th>
                          <th  colspan="1" rowspan="1" aria-controls="example2" tabindex="0" class="sorting">P. mayoreo</th>
                          <th  colspan="1" rowspan="1" aria-controls="example2" tabindex="0" class="sorting">P. menudeo</th>
                          <th  colspan="1" rowspan="1" aria-controls="example2" tabindex="0" class="sorting">F. alta</th>
                          <th  colspan="1" rowspan="1" aria-controls="example2" tabindex="0" class="sorting">F. baja</th>
                      </tr>
                    </tfoot>
                  </div><!-- /.box-body -->
              </div>
    
                 <!-- jQuery 2.1.4 -->
    <script src="../shared/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../shared/bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="../shared/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="../shared/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="../shared/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../shared/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../shared/dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../shared/dist/js/demo.js"></script>
    <!-- page script -->
    <script>
      $(function () {
        $("#example1").DataTable();
        $("#example2").DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>