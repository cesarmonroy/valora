<?php
    
    require_once '../applibs/dpAPI.php';
    require_once '../applibs/coreAPI.php';

    $conf = new coreconfig();
    $cry = new corecrypt();
    $data = new coredb();
    $log = new corelog();
    $mview = new setviews();
    $core = new corelogic();
    $messages = new system_messages();
    

    
        $nivel_acceso=4;
	if ($nivel_acceso <= $_SESSION['usuario_nivel']){
		header ("Location: $redir?error_login=5");
		exit;
	}
        
        
            $usuario = $_SESSION['usuario_login'];
            $profile_image=$core->return_image_profile($usuario);
        
        include_once $mview->set_main_views($_SESSION['usuario_nivel']);
        
        
?>