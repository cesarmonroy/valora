<?php
   
    require_once '../applibs/dpAPI.php';
    require_once '../applibs/coreAPI.php';

    $conf = new coreconfig();
    $cry = new corecrypt();
    $data = new coredb();
    $log = new corelog();
    $mview = new setviews();
    $system_messages = new system_messages();
    
    $usuario = $_SESSION['usuario_login'];
    
        $nivel_acceso=1;
	if ($nivel_acceso <= $_SESSION['usuario_nivel']){
		header ("Location: $redir?error_login=5");
		exit;
	}
        

        $id = $_GET['id'];
        $autor=$_POST['autor'];
        $tipo=$_POST['tipo'];
        $descripcion = strtoupper($_POST['descripcion']);
        $costo=$_POST['costo'];
        $precio_mayoreo=$_POST['precio_mayoreo'];
        $precio_menudeo=$_POST['precio_menudeo'];
        
        
        
    if($autor!=NULL){
        $sql = "UPDATE `valora_main`.`productos` SET `nombre`='$nombre' WHERE `id` LIKE '$id';";
        $data->todo($sql);
        $string = "Cambio de datos de nombre del autor ".$id." a:".$nombre;
        $log->putlog($usuario, $string);
    }
    if($apellidos!=NULL){
        $sql = "UPDATE `valora_main`.`autores` SET `apellidos`='$apellidos' WHERE `id` LIKE '$id';";
        $data->todo($sql);
        $string = "Cambio de datos de apellidos del autor ".$id." a:".$apellidos;
        $log->putlog($usuario, $string);
    }
               /**
                * Hay que revisar el code a partir de aqui
                */
    $result = $data->query("SELECT id,id_autor,tipo_producto,descripcion,status,token,costo,precio_mayoreo,precio_menudeo,f_alta,f_baja FROM productos ORDER BY ID ASC");
                        while( $array_datos = mysql_fetch_array($result)){
                            $autor=$data->extract_data("SELECT nombre FROM autores WHERE id LIKE '$array_datos[1]'")." ".$data->extract_data("SELECT apellidos FROM autores WHERE id LIKE '$array_datos[1]'");
                             echo "<tr class='odd' role='row'><td class='sorting_1'><a href=producto.modificar.php?id=$array_datos[0]><span class='glyphicon glyphicon-edit'></span> $array_datos[0]</td><td>$array_datos[1]</td><td>$array_datos[2]</td><td>$array_datos[3]</td><td>$array_datos[4]</td><td>$array_datos[5]</td><td>$array_datos[6]</td><td>$array_datos[7]</td><td>$array_datos[8]</td><td>$array_datos[9]</td><td>$array_datos[10]</td></tr>";
                        }
                       
?>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../shared/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../shared/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../shared/dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  
  <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4>	<i class="icon fa fa-check"></i> Alert!</h4>
                    <?php echo $system_messages->message_change_success;?>
                  </div>
  
  <div class="box box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-yellow">
                  <div class="widget-user-image">
                    Autor No. <?php echo $id?>
                  </div><!-- /.widget-user-image -->
                  <h3 class="widget-user-username"><?php echo $nombre." ".$apellidos ?></h3>
                  <h5 class="widget-user-desc"><?php echo $token ?></h5>
                </div>
                <div class="box-footer no-padding">
                  <ul class="nav nav-stacked">
                    <li><a href="#">Fecha de alta <span class="badge bg-blue"><?php echo $f_alta?></span></a></li>
                    <li><a href="#">Fecha de baja <span class="badge bg-aqua"><?php echo $f_baja?></span></a></li>
                    <li><a href="#">Status <span class="badge bg-green"><?php echo $status?></span></a></li>
                  </ul>
                </div>
</div>
  
  
  
  <!-- jQuery 2.1.4 -->
    <script src="../shared/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../shared/bootstrap/js/bootstrap.min.js"></script>
    <!-- Slimscroll -->
    <script src="../shared/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../shared/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../shared/dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../shared/dist/js/demo.js"></script>
  