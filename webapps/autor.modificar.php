<?php
   
    require_once '../applibs/dpAPI.php';
    require_once '../applibs/coreAPI.php';

    $conf = new coreconfig();
    $cry = new corecrypt();
    $data = new coredb();
    $log = new corelog();
    $mview = new setviews();
    $usuario = $_SESSION['usuario_login'];
    
        $nivel_acceso=1;
	if ($nivel_acceso <= $_SESSION['usuario_nivel']){
		header ("Location: $redir?error_login=5");
		exit;
	}
        

    $id = $_GET['id'];
             
                        $result = $data->query("SELECT id,nombre,apellidos,f_alta,f_baja,status,token FROM autores WHERE id LIKE '$id'");
                        while( $array_datos = mysql_fetch_array($result)){
                             $autor_id = $array_datos[0];
                             $nombre = $array_datos[1];
                             $apellidos = $array_datos[2];
                             $f_alta = $array_datos[3];
                             $f_baja = $array_datos[4];
                             $status = $array_datos[5];
                             $token = $array_datos[6];
                        }
                       
    $string = "Solicita el cambio de datos para el autor ID:".$id;
    $log->putlog($usuario, $string);                    
                        
?>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../shared/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../shared/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../shared/dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  
  
  <div class="box box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-yellow">
                  <div class="widget-user-image">
                    Autor No. <?php echo $id?>
                  </div><!-- /.widget-user-image -->
                  <h3 class="widget-user-username"><?php echo $nombre." ".$apellidos ?></h3>
                  <h5 class="widget-user-desc"><?php echo $token ?></h5>
                </div>
                <div class="box-footer no-padding">
                  <ul class="nav nav-stacked">
                    <li><a href="#">Fecha de alta <span class="badge bg-blue"><?php echo $f_alta?></span></a></li>
                    <li><a href="#">Fecha de baja <span class="badge bg-aqua"><?php echo $f_baja?></span></a></li>
                    <li><a href="#">Status <span class="badge bg-green"><?php echo $status?></span></a></li>
                  </ul>
                </div>
</div>
  
  <h2 class="page-header">Modificar datos</h2>
  
  <form method="POST" action="autor.modificar.2.php?id=<?php echo $id;?>&">
  <div class="form-group">
                      <label>Nombre</label>
                      <input style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABHklEQVQ4EaVTO26DQBD1ohQWaS2lg9JybZ+AK7hNwx2oIoVf4UPQ0Lj1FdKktevIpel8AKNUkDcWMxpgSaIEaTVv3sx7uztiTdu2s/98DywOw3Dued4Who/M2aIx5lZV1aEsy0+qiwHELyi+Ytl0PQ69SxAxkWIA4RMRTdNsKE59juMcuZd6xIAFeZ6fGCdJ8kY4y7KAuTRNGd7jyEBXsdOPE3a0QGPsniOnnYMO67LgSQN9T41F2QGrQRRFCwyzoIF2qyBuKKbcOgPXdVeY9rMWgNsjf9ccYesJhk3f5dYT1HX9gR0LLQR30TnjkUEcx2uIuS4RnI+aj6sJR0AM8AaumPaM/rRehyWhXqbFAA9kh3/8/NvHxAYGAsZ/il8IalkCLBfNVAAAAABJRU5ErkJggg==&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;" class="form-control" placeholder="Enter ..." type="text" name="nombre">
                    </div>
  <div class="form-group">
                      <label>Apellidos</label>
                      <input style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABHklEQVQ4EaVTO26DQBD1ohQWaS2lg9JybZ+AK7hNwx2oIoVf4UPQ0Lj1FdKktevIpel8AKNUkDcWMxpgSaIEaTVv3sx7uztiTdu2s/98DywOw3Dued4Who/M2aIx5lZV1aEsy0+qiwHELyi+Ytl0PQ69SxAxkWIA4RMRTdNsKE59juMcuZd6xIAFeZ6fGCdJ8kY4y7KAuTRNGd7jyEBXsdOPE3a0QGPsniOnnYMO67LgSQN9T41F2QGrQRRFCwyzoIF2qyBuKKbcOgPXdVeY9rMWgNsjf9ccYesJhk3f5dYT1HX9gR0LLQR30TnjkUEcx2uIuS4RnI+aj6sJR0AM8AaumPaM/rRehyWhXqbFAA9kh3/8/NvHxAYGAsZ/il8IalkCLBfNVAAAAABJRU5ErkJggg==&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;" class="form-control" placeholder="Enter ..." type="text" name="apellidos">
                    </div>
  <div align="center">
        <button type="submit" class="btn btn-primary">
            <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Modificar
          </button>
            &nbsp; &nbsp;
            <button type="reset" class="btn btn-primary">
            <span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Limpiar
          </button>
        </div>
  
  </form>
  
  <!-- jQuery 2.1.4 -->
    <script src="../shared/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../shared/bootstrap/js/bootstrap.min.js"></script>
    <!-- Slimscroll -->
    <script src="../shared/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../shared/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../shared/dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../shared/dist/js/demo.js"></script>
  