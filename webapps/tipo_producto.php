<?php
    
    require_once '../applibs/dpAPI.php';
    require_once '../applibs/coreAPI.php';

    $conf = new coreconfig();
    $cry = new corecrypt();
    $data = new coredb();
    $log = new corelog();
    $mview = new setviews();
    $usuario = $_SESSION['usuario_login'];
    $core = new corelogic();
    
        $nivel_acceso=1;
	if ($nivel_acceso <= $_SESSION['usuario_nivel']){
		header ("Location: $redir?error_login=5");
		exit;
	}
        
?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    

    <!-- Bootstrap core CSS -->
    
    <link href="../shared/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!-- Custom styles for this template -->
    
    <link href="../shared/bootstrap/css/dashboard.css" rel="stylesheet" type="text/css"/>
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    
    <script src="../shared/bootstrap/js/ie-emulation-modes-warning.js" type="text/javascript"></script>
    
    <?php
        
        $tipo_producto = strtoupper($_POST['tipo_producto']);
        if ($tipo_pago != NULL){
            
            $check=$data->validate_data("c_tipo_producto", "descripcion", $tipo_producto);
            if ($check == 0){
                //no existe
                $status="1";
                $token = $core->create_token($tipo_producto);
                $data->todo("INSERT INTO c_tipo_pproducto(descripcion,status,token) VALUES('$tipo_pproducto','$status','$token')");
                echo "<div class='alert alert-success' role='alert'><strong>Ok!</strong> Este tipo de producto ya se ha registrado.</div>";
            }else{
                //si existe
                echo "<div class='alert alert-danger' role='alert'><strong>Oh no!</strong> Este tipo de producto ya existe.</div>";
            }
            
            
        }

    ?>
    <div align="center" style="width:90%">
    <div class="panel panel-primary">
            <div class="panel-heading">
              <h3 class="panel-title">Agregar tipo de producto</h3>
            </div>
            <div class="panel-body">
                <form action="tipo_producto.php" method="POST">
              <div class="input-group">
                  <input type="text" class="form-control" placeholder="Nuevo tipo..." name="tipo_producto">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="send"><span class="glyphicon glyphicon-plus-sign"></span>  Agregar</button>
                </span>
              </div>
                    </form>
            </div>
    </div>
    
    
    <div class="row">
        <div class="col-md-8"><h4>Tipos de producto</h4></div>    
        <div class="col-md-4">
            <h4><h4><a href="javascript:window.print()" border="0"><span class="label label-primary"> <span class="glyphicon glyphicon-print"></span> Imprimir</span></a> &nbsp;
            
        </div>
      </div>
    
           <table class="table table-striped">
            <thead>
              <tr>
                <th>ID</th>
                <th>Descripción</th>
                <th>Status</th>
                <th>TOKEN</th>
              </tr>
            </thead>
            <tbody>
              <?php
              
                $result = $data->query("SELECT id,descripcion,status,token FROM c_tipo_producto ORDER BY ID ASC");
		while( $array_datos = mysql_fetch_array($result)){
                     echo "<tr><td><a href=tipos_producto.modificar.php?id=$array_datos[0]><span class='glyphicon glyphicon-edit'></span> $array_datos[0]</td><td>$array_datos[1]</td><td>$array_datos[2]</td><td>$array_datos[3]</td></tr>";
                }
                
              ?>
            </tbody>
          </table>
</div>