<?php
    
    require_once '../applibs/dpAPI.php';
    require_once '../applibs/coreAPI.php';

    $conf = new coreconfig();
    $cry = new corecrypt();
    $data = new coredb();
    $log = new corelog();
    $mview = new setviews();
    $usuario = $_SESSION['usuario_login'];
    
        $nivel_acceso=1;
	if ($nivel_acceso <= $_SESSION['usuario_nivel']){
		header ("Location: $redir?error_login=5");
		exit;
	}
        
?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <!-- Bootstrap core CSS -->
    
    <link href="../shared/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!-- Custom styles for this template -->
    
    <link href="../shared/css/dashboard.css" rel="stylesheet" type="text/css"/>
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    
    <script src="../shared/js/ie-emulation-modes-warning.js" type="text/javascript"></script>
    
        <?php

        $nombre = strtoupper($_POST['nombre']);
        $apellidos = strtoupper($_POST['apellidos']);
        
        
        if ($nombre != NULL and $apellidos != NULL){
            
            $check=$data->validate_data("autores", "apellidos", $apellidos);
            if ($check == 0){
                
                $check2=$data->validate_data("autores", "nombre", $nombre);
                if(check2 == 0){
                    //no existe
                    $status="1";
                    $token = $core->create_token($apellidos.$nombre);
                    $data->todo("INSERT INTO autores(nombre,apellidos,f_alta,status,token) VALUES('$nombre','$apellidos',CURDATE(),'$status','$token')");
                    echo "<div class='alert alert-success' role='alert'><strong>Ok!</strong> Este autor ya se ha registrado.</div>";
                }else{
                    //si existe
                    echo "<div class='alert alert-danger' role='alert'><strong>Oh no!</strong> Este autor ya existe.</div>";
                }
   
            }else{
                //si existe
                echo "<div class='alert alert-danger' role='alert'><strong>Oh no!</strong> Este autor ya existe.</div>";
            }
            
            
        }

    ?>
    
    <div align="right">
            <a href="autores.list.php" target="target" border="0"><span class="label label-primary"> <span class="glyphicon-arrow-left"></span> Regresar</span></a>
    </div>
    
    <form action="crear.autor.php" method="POST">
          
    <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title">Agregar Autor</h3>
            </div>
      
            <div class="panel-body">

                <label>Nombre</label>
                <input type="text" class="form-control" placeholder="nombre" name="nombre"><BR>
                <label>Apellidos</label>
                <input type="text" class="form-control" placeholder="apellidos" name="apellidos">
                
            </div>
          </div>
        
        
        
        <div align="center">
        <button type="submit" class="btn btn-primary">
            <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Agregar
          </button>
            &nbsp; &nbsp;
            <button type="reset" class="btn btn-primary">
            <span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Limpiar
          </button>
        </div>
    </form>
    
   