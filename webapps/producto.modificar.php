<?php
   
    require_once '../applibs/dpAPI.php';
    require_once '../applibs/coreAPI.php';

    $conf = new coreconfig();
    $cry = new corecrypt();
    $data = new coredb();
    $log = new corelog();
    $mview = new setviews();
    $usuario = $_SESSION['usuario_login'];
    
        $nivel_acceso=1;
	if ($nivel_acceso <= $_SESSION['usuario_nivel']){
		header ("Location: $redir?error_login=5");
		exit;
	}
        

    $id = $_GET['id'];
             
                        $result = $data->query("SELECT id,id_autor,tipo_producto,descripcion,status,token,costo,precio_mayoreo,precio_menudeo,f_alta,f_baja FROM productos ORDER BY ID ASC");
                        while( $array_datos = mysql_fetch_array($result)){
                            $autor=$data->extract_data("SELECT nombre FROM autores WHERE id LIKE '$array_datos[1]'")." ".$data->extract_data("SELECT apellidos FROM autores WHERE id LIKE '$array_datos[1]'");
                            $product_id=$array_datos[0];
                            $tipo_producto = $array_datos[2];
                            $descripcion=$array_datos[3];
                            $status=$array_datos[4];
                            $token=$array_datos[5];
                            $costo=$array_datos[6];
                            $precio_mayoreo=$array_datos[7];
                            $precio_menudeo=$array_datos[8];
                            $f_alta=$array_datos[9];
                            $f_baja=$array_datos[10];
                        }
                        
    $string = "Solicita el cambio de datos para el producto ID:".$id;
    $log->putlog($usuario, $string);                    
                        
?>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../shared/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../shared/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../shared/dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  
  
  <div class="box box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-yellow">
                  <div class="widget-user-image">
                    Producto No. <?php echo $id?>
                  </div><!-- /.widget-user-image -->
                  <h3 class="widget-user-username"><?php echo $descripcion ?></h3>
                  <h5 class="widget-user-desc">Del autor: <?php echo $autor ?></h5>
                </div>
                <div class="box-footer no-padding">
                  <ul class="nav nav-stacked">
                    <li><a href="#">Fecha de alta <span class="badge bg-blue"><?php echo $f_alta?></span></a></li>
                    <li><a href="#">Fecha de baja <span class="badge bg-aqua"><?php echo $f_baja?></span></a></li>
                    <li><a href="#">Status <span class="badge bg-green"><?php echo $status?></span></a></li>
                    <li><a href="#">TOKEN <span class="badge bg-green"><?php echo $token?></span></a></li>
                    <li><a href="#">Costo <span class="badge bg-green">$ <?php echo $costo?></span></a></li>
                    <li><a href="#">Precio Mayoreo<span class="badge bg-green">$ <?php echo $precio_mayoreo?></span></a></li>
                    <li><a href="#">Precio Menudeo <span class="badge bg-green">$ <?php echo $precio_menudeo?></span></a></li>
                  </ul>
                </div>
</div>
  
  <h2 class="page-header">Modificar datos</h2>
  
  <form action="producto.modificar.2.php" method="POST">
    <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title">Agregar Producto: Información Base</h3>
            </div>
            <div class="panel-body">
                <label>Autor</label>
                <select class="form-control" placeholder="Autor" name="autor">
                    <option>&nbsp</option>
                    <?php
                    
                        $result = $data->query("SELECT id,apellidos,nombre FROM autores ORDER BY ID ASC");
                        while( $array_datos = mysql_fetch_array($result)){
                             echo "<option value=$array_datos[0]>$array_datos[1], $array_datos[2] [ID:$array_datos[0]]</option>";
                        }
                    
                    ?>
                </select>
                
                <div class="alert alert-warning" role="alert">
                    <span class="glyphicon glyphicon-pushpin"></span> &nbsp; Si no aparece el autor que busca favor de <a href="crear.autor.php">darlo de alta</a> 
                  </div>
                
                <label>Tipo</label>
                <select class="form-control" placeholder="Tipo" name="tipo">
                    <option>&nbsp</option>
                    <?php
                        $status=1;
                        $result = $data->query("SELECT descripcion FROM c_tipo_producto WHERE status LIKE '$status' ORDER BY ID ASC");
                        while( $array_datos = mysql_fetch_array($result)){
                             echo "<option value=$array_datos[0]>$array_datos[0]</option>";
                        }
                    
                    ?>
                </select>
                <div class="alert alert-warning" role="alert">
                    <span class="glyphicon glyphicon-pushpin"></span> &nbsp; Si no aparece el tipo de producto que busca favor de <a href="tipo_producto.php">darlo de alta</a> 
                  </div>
                
                <label>Descripción del producto</label>
                <input type="text" class="form-control" placeholder="Descripción" name="descripcion">
                
            </div>
          </div>
        
        <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title">Información de costos</h3>
            </div>
            <div class="panel-body">
              <div class="input-group">
                 <span class="input-group-addon">Costo $</span>
                 <input type="text" class="form-control" aria-label="costo" name="costo">
                
              </div>
                <BR>
                <div class="input-group">
                 <span class="input-group-addon">Precio Mayoreo $</span>
                 <input type="text" class="form-control" aria-label="precio mayoreo" name="precio_mayoreo">
                
              </div>
                <BR>
                <div class="input-group">
                 <span class="input-group-addon">Precio Menudeo $</span>
                 <input type="text" class="form-control" aria-label="precio menudeo" name="precio_menudeo">
                
              </div>
            </div>
          </div>
        
        <div align="center">
        <button type="submit" class="btn btn-primary">
            <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Agregar
          </button>
            &nbsp; &nbsp;
            <button type="reset" class="btn btn-primary">
            <span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Limpiar
          </button>
        </div>
    </form>
  
  <!-- jQuery 2.1.4 -->
    <script src="../shared/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../shared/bootstrap/js/bootstrap.min.js"></script>
    <!-- Slimscroll -->
    <script src="../shared/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../shared/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../shared/dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../shared/dist/js/demo.js"></script>
  