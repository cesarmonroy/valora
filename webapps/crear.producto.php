<?php
    
    require_once '../applibs/dpAPI.php';
    require_once '../applibs/coreAPI.php';

    $conf = new coreconfig();
    $cry = new corecrypt();
    $data = new coredb();
    $log = new corelog();
    $mview = new setviews();
    $usuario = $_SESSION['usuario_login'];
    
        $nivel_acceso=1;
	if ($nivel_acceso <= $_SESSION['usuario_nivel']){
		header ("Location: $redir?error_login=5");
		exit;
	}
        
?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <!-- Bootstrap core CSS -->
    
    <link href="../shared/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!-- Custom styles for this template -->
    
    <link href="../shared/css/dashboard.css" rel="stylesheet" type="text/css"/>
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    
    <script src="../shared/js/ie-emulation-modes-warning.js" type="text/javascript"></script>
    
        <?php

        $autor=$_POST['autor'];
        $tipo=$_POST['tipo'];
        $descripcion = strtoupper($_POST['descripcion']);
        $costo=$_POST['costo'];
        $precio_mayoreo=$_POST['precio_mayoreo'];
        $precio_menudeo=$_POST['precio_menudeo'];
        
        if ($autor != NULL){
            
            $check=$data->validate_data("productos", "descripcion", $descripcion);
            if ($check == 0){
                //no existe
                $status="1";
                $token = $core->create_token($descripcion);
                $data->todo("INSERT INTO productos(autor,tipo_producto,descripcion,status,token,costo,precio_mayoreo,precio_menudeo,f_alta) VALUES('$autor','$tipo','$descripcion','$status','$token','$costo','$precio_mayoreo','$precio_menudeo',CURDATE())");
                echo "<div class='alert alert-success' role='alert'><strong>Ok!</strong> Este producto ya se ha registrado.</div>";
            }else{
                //si existe
                echo "<div class='alert alert-danger' role='alert'><strong>Oh no!</strong> Este producto ya existe.</div>";
            }
            
            
        }

    ?>
    
    <form action="crear.producto.php" method="POST">
    <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title">Agregar Producto: Información Base</h3>
            </div>
            <div class="panel-body">
                <label>Autor</label>
                <select class="form-control" placeholder="Autor" name="autor">
                    <option>&nbsp</option>
                    <?php
                    
                        $result = $data->query("SELECT id,apellidos,nombre FROM autores ORDER BY ID ASC");
                        while( $array_datos = mysql_fetch_array($result)){
                             echo "<option value=$array_datos[0]>$array_datos[1], $array_datos[2] [ID:$array_datos[0]]</option>";
                        }
                    
                    ?>
                </select>
                
                <div class="alert alert-warning" role="alert">
                    <span class="glyphicon glyphicon-pushpin"></span> &nbsp; Si no aparece el autor que busca favor de <a href="crear.autor.php">darlo de alta</a> 
                  </div>
                
                <label>Tipo</label>
                <select class="form-control" placeholder="Tipo" name="tipo">
                    <option>&nbsp</option>
                    <?php
                        $status=1;
                        $result = $data->query("SELECT descripcion FROM c_tipo_producto WHERE status LIKE '$status' ORDER BY ID ASC");
                        while( $array_datos = mysql_fetch_array($result)){
                             echo "<option value=$array_datos[0]>$array_datos[0]</option>";
                        }
                    
                    ?>
                </select>
                <div class="alert alert-warning" role="alert">
                    <span class="glyphicon glyphicon-pushpin"></span> &nbsp; Si no aparece el tipo de producto que busca favor de <a href="tipo_producto.php">darlo de alta</a> 
                  </div>
                
                <label>Descripción del producto</label>
                <input type="text" class="form-control" placeholder="Descripción" name="descripcion">
                
            </div>
          </div>
        
        <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title">Información de costos</h3>
            </div>
            <div class="panel-body">
              <div class="input-group">
                 <span class="input-group-addon">Costo $</span>
                 <input type="text" class="form-control" aria-label="costo" name="costo">
                
              </div>
                <BR>
                <div class="input-group">
                 <span class="input-group-addon">Precio Mayoreo $</span>
                 <input type="text" class="form-control" aria-label="precio mayoreo" name="precio_mayoreo">
                
              </div>
                <BR>
                <div class="input-group">
                 <span class="input-group-addon">Precio Menudeo $</span>
                 <input type="text" class="form-control" aria-label="precio menudeo" name="precio_menudeo">
                
              </div>
            </div>
          </div>
        
        <div align="center">
        <button type="submit" class="btn btn-primary">
            <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Agregar
          </button>
            &nbsp; &nbsp;
            <button type="reset" class="btn btn-primary">
            <span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Limpiar
          </button>
        </div>
    </form>
    
   