<?php

    require_once '../core/dpcore.config.php';
    require_once '../core/dpcore.compress.php';
    require_once '../core/dpcore.crypt.php';
    require_once '../core/dpcore.db.php';
    require_once '../core/dpcore.log.php';
    require_once '../core/dpcore.search.php';
    require_once '../securitycore/dparts.security.inc.php';

?>
