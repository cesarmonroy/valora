<?php

   require_once './core/dpcore.config.php';
   $config = new coreconfig();
   require_once './applibs/core.messages.php';
   $system_messages = new system_messages();
   
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $config->page_title;?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="./shared/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="./shared/dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="./shared/plugins/iCheck/square/blue.css">

  </head>
  <body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">
        <a href="http://valora.kafkadigital.com"><b><?php echo $config->page_title;?></b></a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
          <div align="center">
            <?php
                    // Mostrar error de Autentificaci?n                          
                    include_once ("./securitycore/dparts.security.messages.inc.php");
                    if (isset($_GET['error_login'])){
                       $error=$_GET['error_login'];
                       echo "Error: $error_login_ms[$error]";
                    }
            ?>
        </div>
        <p class="login-box-msg"><?php echo $system_messages->message_login; ?></p>
        <form action="./webapps/main.php" method="post">
          <div class="form-group has-feedback">
              <input type="text" class="form-control" placeholder="Email" name="user">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
              <input type="password" class="form-control" placeholder="Password" name="pass">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-8">
              
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Entrar</button>
            </div><!-- /.col -->
          </div>
        </form>

        <a href="#"><?php $system_messages->message_forget_pass;?></a><br>
        

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="./shared/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="./shared/bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="./shared/plugins/iCheck/icheck.min.js"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  </body>
</html>
